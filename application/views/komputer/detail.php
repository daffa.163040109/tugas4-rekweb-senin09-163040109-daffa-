<div class="container mt-5">
	<div class="card" style="width: 18rem;">
		<div class="card-body">
			<h5 class="card-title"><?= $komputer[0]->name ?></h5>
			<h6 class="card-subtitle mb-2 text-muted"><?= $komputer[0]->price ?></h6>
			<p class="card-text"><?= $komputer[0]->stok ?></p>
			<p class="card-text"><?= $komputer[0]->description ?></p>
			<a href="<?= site_url('komputer') ?>" class="card-link">Kembali</a>
		</div>
	</div>
</div>