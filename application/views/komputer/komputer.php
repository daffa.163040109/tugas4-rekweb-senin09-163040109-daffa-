<!-- Modal Tambah -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Perangkat</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" id="formTambah" method="post">
					<div class="form-group">
						<label for="nama">Nama Perangkat</label>
						<input type="text" class="form-control" name="name" id="name"
							   placeholder="Masukan Nama Perangkat">
					</div>
					<div class="form-group">
						<label for="jenis">Harga Perangkat</label>
						<input type="text" class="form-control" name="price" id="price"
							   placeholder="Masukan Harga Perangkat">
					</div>
					<div class="form-group">
						<label for="gambar">Gambar</label>
						<input type="text" class="form-control" name="image" id="image"
							   placeholder="Masukan Gambar">
					</div>
					<div class="form-group">
						<label for="stok">Stok</label>
						<input type="stok" class="form-control" id="stok" aria-describedby="namaHelp"
							   name="stok" placeholder="Masukan Stok">
					</div>
					<div class="form-group">
						<label for="harga">Deskripsi</label>
						<input type="text" class="form-control" name="description" id="description"
							   placeholder="Masukan Deskripsi">
					</div>
					
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" id="submitAdd" class="btn btn-primary" name="add">Simpan</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- Modal Update -->
<div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel1">Update Perangkat
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" id="formUpdate" method="post">
					<input type="hidden" name="product_id" id="product_id">
					<div class="form-group">
						<label for="NamaUpdate">Nama</label>
						<input type="text" class="form-control" id="namaUpdate" aria-describedby="namaHelp"
							   name="NamaUpdate" placeholder="Masukan Nama"  value="<?= $aks["jenis"]; ?>">
					</div>
					<div class="form-group">
						<label for="jenisUpdate">Jenis 
						</label>
						<input type="text" value="" class="form-control" name="jenisUpdate" id="jenisUpdate"
							   placeholder="Masukan Jenis Aksesoris
							   ">
					</div>
					<div class="form-group">
						<label for="jenisUpdate">Stok</label>
						<input type="jenis" value="" class="form-control" id="jenisUpdate"
							   aria-describedby="jenisHelp" name="jenisUpdate" placeholder="Masukan Stok">
					</div>
					<div class="form-group">
						<label for="hargaUpdate">Harga</label>
						<input type="text" value="" class="form-control" name="hargaUpdate" id="hargaUpdate"
							   placeholder="Masukan Harga">
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="add" id="submitUpdate">Simpan</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="container mt-5">
	<div class="row">
		<div class="col-12">
			<h3 style="text-align: center;">Daftar Komputer</h3>
			<button class="btn btn-success btn-sm" id="buttonAdd" data-toggle="modal" data-target="#exampleModal">
				Tambah
			</button>
			<div class="col-3" style="float: right; position: relative; left: 15px">
				<form action="<?= site_url('/komputer/search/') ?>" method="get">
					<div class="form-group">
						<div class="input-group">
							<input type="text" name="keyword" class="form-control" placeholder="Search...">
							<div class="input-group-btn">
								<button type="submit" class="btn btn-primary"
										style="border-top-left-radius: 0px; border-bottom-left-radius: 0px;">Search
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<table class="table table-striped table-bordered" id="mytable" style="margin-top:20px ">
				<thead class="thead-dark">
					<tr>
						<th scope="col" style="text-align: center;">No</th>
						<th scope="col" style="text-align: center;">Name</th>
						<th scope="col" style="text-align: center;">Price</th>
						<th scope="col" style="text-align: center;">Image</th>
						<th scope="col" style="text-align: center;">Stok</th>
						<th scope="col" style="text-align: center;">Description</th>
						<th scope="col" style="text-align: center;">Action</th>
						
					</tr>
				</thead>
				<?php if(empty($komp)): ?>
					<tr>
						<td colspan="6" style="text-align: center;">Maaf data yang anda cari tidak ditemukan</td>
					</tr>
				<?php else : ?>
					<?php $no = 1 ?>
					<?php foreach($komp as $key) : ?>
						<tr>
							<td><?= $no++ ?></td>
							<td><?= $key->name ?></td>
							<td><?= $key->price ?></td>
							<td><img src="<?= base_url('assets/img/aksesoris'); ?>"></td>
							<td><?= $key->stok ?></td>
							<td><?= $key->description ?></td>
							
							<td style="text-align: center;">
						


						 		<a href="<?= site_url('komputer/detail/' . $key->product_id) ?>" class="btn btn-sm btn-primary">Detail</a>
								<a href="<?= site_url('komputer/tambah/' . $key->product_id) ?>" class="btn btn-sm btn-success update" id="<?= $key->product_id ?>">Update</a>
								<a href="" id="<?= $key->product_id ?>" class="btn btn-sm btn-danger delete" >Delete</a>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif ?>
			</table>
		</div>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?= base_url('assets/js/sweetalert.min.js') ?>"></script>
<script>
	$(document).ready(function () {
		
		$("#submitAdd").click(function (e) {
			e.preventDefault();
			$.ajax({
				url: "<?= site_url('komputer/create') ?>",
				type: "POST",
				dataType: "JSON",
				data: $("#formTambah").serialize(),
				success: function(){
					$('#exampleModal').modal('hide');
					swal({
						title: "Success",
						text: "Data berhasil disimpan",
						icon: "success",
						buttons: false,
					});
					setTimeout(function(){
						location.reload();
					}, 2000);
				},
				error: function(xhr, status, error){
					alert(status + " : " + error);
				}
			});
		});

		$(".update").click(function (e) {
			e.preventDefault();
			$("#modalUpdate").modal('show');
			var id = $(this).attr("id");
			$.ajax({
				url: "<?php echo site_url('aksesoris/edit/') ?>",
				type: "POST",
				data: "id=" + id,
				dataType: "json",
				success: function(data){
					if(data.status == 200){
						$("#id_aks").val(data.aks[0].id);
						$("#namaUpdate").val(data.aks[0].nama);
						$("#jenisUpdate").val(data.aks[0].jenis);
						$("#stokUpdate").val(data.aks[0].stok);
						$("#hargaUpdate").val(data.aks[0].harga);
					}
				},
				error: function(xhr, status, error){
					alert(status + " : " + error);
				}
			});
		});

		$("#submitUpdate").click(function (e) {
			e.preventDefault();
			$.ajax({
				url: "<?= site_url('komputer/update') ?>",
				type: "POST",
				data: $("#formUpdate").serialize(),
				dataType: "JSON",
				success: function(){
					$('#exampleModal').modal('hide');
					swal({
						title: "Success",
						text: "Data berhasil disimpan",
						icon: "success",
						buttons: false,
					});
					setTimeout(function(){
						location.reload();
					}, 2000);
				},
				error: function(xhr, status, error){
					alert(status + " : " + error);
				}
			});
		});

		$(".delete").click(function (e) {
			e.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Apakan anda yakin ingin menghapus ?",
				text: "Data yang terhapus tidak bisa dipulihkan",
				icon: "warning",
				buttons: true,
				dangerMode: true;
			})
				.then((willDelete) =>{
					if (willDelete) {
						$.ajax({
							url: "<?= site_url('aksesoris/delete') ?>",
							type: "POST",
							data: "id=" + id,
							dataType: "JSON",
							success: function(data){
								swal({
									title: "Success",
									text: "Data berhasil dihapus",
									icon: "success",
									buttons: false,
								});
								setTimeout(function(){
									location.reload();
								}, 2000);
							},
							error: function(xhr, status, error){
								alert(status + " : " + error);
							}
						});
					}else{
						swal("Batal menghapus data");
					}
				});
	});
</script>


