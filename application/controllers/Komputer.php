 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komputer extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/komputer-api";
	}

	public function index()
	{
		$data['judul'] = 'Latihan Rest API';
		$data['komputer'] = json_decode($this->curl->simple_get($this->API . '/komputer'));
		$data['komp'] = $data['komputer'];
		$data['content'] = 'komputer/komputer';

		$this->load->view('template/template', $data);
	}

	public function detail($id)
	{
		$data['judul'] = 'Latihan Rest API';
		$data['komputer'] = json_decode($this->curl->simple_get($this->API . '/komputer/', array("product_id" => $id)));
		$data['content'] = 'komputer/detail';

		$this->load->view('template/template', $data);
	}

	public function search()
	{
		$search = urldecode($this->input->get("keyword", true));
		$data['judul'] = 'Latihan Rest API';
		$data['aksesoris'] = json_decode($this->curl->simple_get($this->API . '/aksesoris/search/?cari='. urldecode($search)));
		$data['aks'] = $data['aksesoris'];
		$data['content'] = 'aksesoris/aksesoris';

		$this->load->view('template/template', $data);
	}

	public function create()
	{
		
		$nama = $this->input->post('nama');
		$jenis = $this->input->post('jenis');
		$stok = $this->input->post('stok');
		$harga = $this->input->post('harga');
		$gambar = $this->input->post('gambar');

		$data = array(
				'nama' => $nama,
				'jenis' => $jenis,
				'stok' => $stok,
				'harga' => $harga,
				'gambar' => $gambar,
				);
		$this->curl->simple_post($this->API . '/aksesoris/', $data, array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(array("status" => true ));

		

		return "Data Berhasil Masuk";

		
	}

	public function edit()
	{
		$id = $this ->input->post("id");
		$params = array('id' => $id);
		$data['aksesoris'] = json_encode($this->curl->simple_get($this->API . '/aksesoris/', $params ));
		$aksesoris = $data['aksesoris'];
		$json = json_encode(array("status" => 200, "aks" => $aksesoris));
		echo $json;
	}

	public function update()
	{
		$name = $this->input->post('nameUpdate');
		$price = $this->input->post('priceUpdate');
		$image = $this->input->post('imageUpdate');
		$stok = $this->input->post('stokUpdate');
		$description = $this->input->post('descriptionUpdate');
		$id = $this->input->post("product_id");

		$data = array(
				'product_id' => $id,
				'name' => $name,
				'price' => $price,
				'image' => $image,
				'stok' => $stok,
				'description' => $description,
				);
		$this->curl->simple_put($this->API . '/komputer/', $data, array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(array("status" => TRUE ));
	}

	public function delete()
	{
		$id = $this->input->post("id");
		json_decode($this->curl->simple_delete($this->API . '/aksesoris/', array('id' => $id), array(CURLOPT_BUFFERSIZE => 10)));
		echo json_encode(array("status" => TRUE ));
	}
}
